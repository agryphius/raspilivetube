Seit Frühjahr 2020 wird das offiziell unterstützte Betriebssystem "Raspberry Pi OS (previously called Raspbian)" für "Raspberry Pi"-Modelle in der Version 10 Buster angeboten.
Damit wird das video converter Programm ffmpeg ausgeliefert mit der Besonderheit, dass - im Unterschied zu vorherigen Raspbian-Ausgaben - die Unterstützung für OpenMAX/omx bereits enthalten ist. 
Durch OpenMAX/omx kann die in der GPU aller "Raspberry Pi"-Modelle enthaltene H.264-Hardwarebeschleunigung genutzt werden. Dadurch werden selbst generierte h.264/MP4-Videos bei relativ geringer CPU-Belastung möglich. 

ffmpeg ist ein sehr vielseitiges Tool zum Verarbeiten von Video- und Audio-Daten. Die Nutzung fortgeschrittener Funktionen kann durch unzählige Parameter gesteuert werden, was oft auch viel Komplexität mit sich bringt.

**HINWEIS** Debian-11-basierte Images: Mit dem Wechsel der System-Basis von Debian10 auf Debian11 im Herbst 2021 wurde die Unterstützung für OpenMAX/omx entfernt. Dafür wurde auf die V4L2-M2M API umgestellt.
Leider läuft das Encoden in ffmpeg mit diesem neuen Codec (h264_v4l2m2m statt h264_omx) nicht stabil (häufige Kernel crashes auf RP Zero W 32Bit). Und h264_omx ist nicht mehr verwendbar (fehlende Bibliotheken ...).
Das bedeutet, Stand Februar 2022, für ein funktionierendes System die **Images vor 20211030 verwenden**. Das aktuellste brauchbare Image ist **hier zu finden**: 
https://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2021-05-28/

Die in diesem Projekt angebotenen Dateien sind im Wesentlichen Skripte, die das ffmpeg-Programm mit geeigneten Parametern aufrufen und eine dauerhafte Ausführung sicherstellen. 
Die Parameter erzeugen einen RTMP-Livestream, so dass er von YouTube verarbeitet werden kann.

Folgende fünf Dateien sind enthalten:

1. /boot/config.txt
   Um zwei Parameter erweiterte originale config-Datei, so dass 
   eine angeschlossene Kamera sofort aktiviert ist

2. /boot/config_youtube_lifestream.txt
   In dieser Textdatei wird der Streaming-Schlüssel für Youtube eingetragen.
   Und es können ein paar Individualierungen des Videostreams eingestellt werden, bspw.
   ein mitlaufender Zeitstempel oder ein eingeblendetes Logo, und jeweils die Position im Stream.

3. /rootfs/etc/crontab
   Um ein paar Einträge erweiterte originale crontab-Datei.
   Dadurch werden die ffmpeg-Skripte beim Booten ausgeführt.

4. /rootfs/home/pi/youtube_livestreaming_restarter.sh
   Skript zum Überwachen des ffmpeg-Prozesses und Neustarten bei zu häufigen Fehlern.

5. /rootfs/home/pi/youtube_livestreaming.sh
   DAS Skript.
   Wird beim Booten via /etc/crontab ausgeführt.
   Setzt die Zeitzone auf Berlin/Deutschland.
   Installiert ffmpeg, falls noch nicht vorhanden.
   Liest die Datei /boot/config_youtube_lifestream.txt ein.
   Ruft in eine while-Schleife ffmpeg mit den Youtube-Livestream-Parametern auf.









#!/bin/bash

echo "$0 Start Ueberwachung raspberry/ffmpeg: $(/bin/date)" >> /home/pi/youtube_ffmpeg.log

# Beim Start erstmal 30 sec warten ...
sleep 30

ERRCOUNT=0

while true
do
  FFMPEGPID=$(/bin/pidof ffmpeg || echo 0)
  if [ ${FFMPEGPID} -ne 0 ] ; then
    # ffmpeg kommt beim Streamen zu Youtube manchmal durcheinander, es werden zwar irgendwelche Daten gestreamt, aber 
    # Youtube kann keinen Videostream draus machen.
    # Einziges messbares Symptom ist dabei, dass die CPU- oder die Memory-Auslastung von ffmpeg dann unter 20% (bzw. MEM unter 15%)liegt.
    # CPU-Last bei top unter 20, aber kurioserweise bei ps ux unter 50 (ca 45%) ...
    # Aber Memory scheint einigermassen gleich bei top und ps zu sein ...
    FFMPEGLINE=$(/bin/ps ux | grep " ${FFMPEGPID} " | grep ffmpeg | sed 's/\ \+/\ /g')
    CPUVAL=$(echo "${FFMPEGLINE}" | cut -d ' ' -f3 | cut -d '.' -f1)
    MEMVAL=$(echo "${FFMPEGLINE}" | cut -d ' ' -f4 | cut -d '.' -f1)
    # Mit Wechsel des ffmpeg-PArameters von -framerate 20 auf -r 20 
    # werden jetzt tatsächlich nur 20fps generiert (-framerate ist wirkungslos), 
    # das bringt weniger CPU-Last mit sich, so dass auch noch mit 47% CPU ein verwertbarer Stream zu Youtube gesendet wird.
    # Mindest-CPU-Wert jetzt also 45 statt 50 ...
    #if [ ${CPUVAL} -lt 50 -o ${MEMVAL} -lt 15 ] ; then
    if [ ${CPUVAL} -lt 45 -o ${MEMVAL} -lt 15 ] ; then
      # Der ffmpeg-Prozess wird hier beendet und automatisch vom anderen livestreaming-Skript wieder gestartet.
      echo "$0 ffmpeg-Prozess ${FFMPEGPID} wird wegen nur ${CPUVAL}% CPU- und ${MEMVAL}% Memory-Auslastung beendet: $(/bin/date)" >> /home/pi/youtube_ffmpeg.log
      /bin/kill ${FFMPEGPID}
    fi
    ERRCOUNT=0
  else
    # Ist ffmpeg schon installiert? Wenn nicht, brauchen wir hier jetzt nix zu machen.
    # Um die Installation kuemmert sich ein anderes Skript. 
    if [ $( /usr/bin/which ffmpeg | /bin/grep -c ffmpeg ) -gt 0 ] ; then 
      # Scheinbar gibt es keinen laufenden ffmpeg-Prozess. Schlecht. 
      # Wir machen einen Neustart, aber erst nach dreimal kein ffmpeg-Prozess ...
      if [ ${ERRCOUNT} -eq 3 ] ; then
        echo "$0 3x kein ffmpeg-Prozess ... Reboot: $(/bin/date)" >> /home/pi/youtube_ffmpeg.log
        sudo reboot ; exit
      fi
      ERRCOUNT=$((ERRCOUNT + 1))
    fi
  fi
  # Wir checken alle 5 Minuten ...
  sleep 300
done


#!/bin/bash

#export LANG=de_DE.UTF-8
#export LANGUAGE=de_DE:de

YOUTUBE_FFMPEG_LOGFILE=/home/pi/youtube_ffmpeg.log
# Logdatei max. 10MB (=10000 blocks). Wenn groesser, dann loesche die ersten 1000 Zeilen
if [ -w "${YOUTUBE_FFMPEG_LOGFILE}" ] ; then
  [ $(ls --size "${YOUTUBE_FFMPEG_LOGFILE}" | cut -d ' ' -f1) -gt 10000 ] && sed -i '1,1000d' "${YOUTUBE_FFMPEG_LOGFILE}"
fi

echo "-----------------------------" >> "${YOUTUBE_FFMPEG_LOGFILE}"
echo "Neustart raspberry/ffmpeg: $(/bin/date)" >> "${YOUTUBE_FFMPEG_LOGFILE}"

# Zeitzone Berlin setzen ...
if [ ! "$(/usr/bin/md5sum /usr/share/zoneinfo/Europe/Berlin | cut -d " " -f 1)" = "$(/usr/bin/md5sum /etc/localtime | cut -d " " -f 1)" ] ; then
  sudo cp /usr/share/zoneinfo/Europe/Berlin /etc/localtime
  echo "$(/bin/date) : Zeitzone /usr/share/zoneinfo/Europe/Berlin nach /etc/localtime kopiert ..." >> "${YOUTUBE_FFMPEG_LOGFILE}"
else
  echo "$(/bin/date) : Zeitzone ist Europe/Berlin ." >> "${YOUTUBE_FFMPEG_LOGFILE}"
fi

# Ist ffmpeg bereits installiert?
ERRCOUNT=0
while [ $( /usr/bin/which ffmpeg | /bin/grep -c ffmpeg ) = 0 ] ; do 
  echo "------ Kein ffmpeg installiert $(date) -------" >> "${YOUTUBE_FFMPEG_LOGFILE}"
  while sudo fuser /var/{lib/{dpkg,apt/lists},cache/apt/archives}/lock >/dev/null 2>&1 ; do sleep 20 ; done
  while true ; do 
    echo "++++++++++++ $(date) starte apt-get --allow-releaseinfo-change update ... +++++++++++" >> "${YOUTUBE_FFMPEG_LOGFILE}"
    echo "$(date) , Job: apt aufraeumen ... sudo apt clean" >> "${YOUTUBE_FFMPEG_LOGFILE}"
    sudo apt clean >> "${YOUTUBE_FFMPEG_LOGFILE}"
    APTGETOUTPUT=$(time sudo /usr/bin/apt-get --allow-releaseinfo-change update)
    echo "${APTGETOUTPUT}" &>> "${YOUTUBE_FFMPEG_LOGFILE}"
    if [ $(echo "${APTGETOUTPUT}" | grep -c "^E") -gt 0 ] ; then
      echo "++++++++++++ $(date) Fehler, also nochmal apt-get --allow-releaseinfo-change update ... +++++++++++" >> "${YOUTUBE_FFMPEG_LOGFILE}"
    else
      break
    fi
  done
  echo "$(date) , Job: apt aufraeumen ... sudo apt clean" >> "${YOUTUBE_FFMPEG_LOGFILE}"
  sudo apt clean >> "${YOUTUBE_FFMPEG_LOGFILE}"
  echo "++++++++++++ $(date) (nach apt-get --allow-releaseinfo-change update) kurz dpkg --configure ... +++++++++++" >> "${YOUTUBE_FFMPEG_LOGFILE}"

  while sudo fuser /var/lib/dpkg/lock-frontend >/dev/null 2>&1 ; do sleep 20 ; done
  # Falls E: dpkg was interrupted, you must manually run 'sudo dpkg --configure -a' to correct the problem.
  time sudo /usr/bin/dpkg --configure --pending &>> "${YOUTUBE_FFMPEG_LOGFILE}"

  echo "++++++++++++ $(date) starte apt-get install ffmpeg ... +++++++++++" >> "${YOUTUBE_FFMPEG_LOGFILE}"
  # plus Paket fonts-smc für Karumbi-Schriftart ...
  APTGETOUTPUT=$(time sudo /usr/bin/apt-get install -y ffmpeg fonts-smc)
  echo "${APTGETOUTPUT}" &>> "${YOUTUBE_FFMPEG_LOGFILE}"
  if [ $(echo "${APTGETOUTPUT}" | grep -c "^E") -gt 0 ] ; then
    echo "++++++++++++ $(date) Fehler bei apt-get install ffmpeg, zuerst apt -y --fix-broken install ... +++++++++++" >> "${YOUTUBE_FFMPEG_LOGFILE}"
    time sudo /usr/bin/apt -y --fix-broken install &>> "${YOUTUBE_FFMPEG_LOGFILE}"
    sudo apt clean >> "${YOUTUBE_FFMPEG_LOGFILE}"
    echo "++++++++++++ $(date) Fehler bei apt-get install ffmpeg, nochmal mit --fix-missing ... +++++++++++" >> "${YOUTUBE_FFMPEG_LOGFILE}"
    time sudo /usr/bin/apt-get install -y ffmpeg fonts-smc --fix-missing &>> "${YOUTUBE_FFMPEG_LOGFILE}"
    sudo apt clean >> "${YOUTUBE_FFMPEG_LOGFILE}"
  fi
  if [ $( /usr/bin/which ffmpeg | /bin/grep -c ffmpeg ) = 0 ] ; then 
    echo "************ $(date) ffmpeg ist NICHT installiert . ***********" >> "${YOUTUBE_FFMPEG_LOGFILE}"
  else
    echo "************ $(date) ffmpeg ist installiert . ***********" >> "${YOUTUBE_FFMPEG_LOGFILE}"
  fi

  if [ ${ERRCOUNT} -eq 3 ] ; then
    echo "$0 3x ffmpeg-Installieren fehlgeschlagen ... Reboot: $(/bin/date)" >> /home/pi/youtube_ffmpeg.log
    sudo reboot ; exit
  fi
  ERRCOUNT=$((ERRCOUNT + 1))
done

# Importiere aktuelle config-Datei fuer ffmpeg und YouTube-Livestream
# DangerDangerDanger: Durch diese Art des includens kann auch boeser bash-Code eingeschleust werden ...
[ ! -r /boot/config_youtube_lifestream.txt ] || . /boot/config_youtube_lifestream.txt

# "${STREAMSCHLUESSEL}" des Livestreams
if [ "${STREAMSCHLUESSEL}xxx" = "xxx" ] ; then
  echo "Kein STREAMSCHLUESSEL festgelegt. Skript wird beendet. $(/bin/date)" >> "${YOUTUBE_FFMPEG_LOGFILE}"
  exit
else
  STREAMURL="rtmp://a.rtmp.youtube.com/live2/${STREAMSCHLUESSEL}"
fi

# Text einblenden ja/nein
if [ ! "${SHOWTEXTJANEIN}xxx" = "xxx" ] ; then
  case "${SHOWTEXTJANEIN}" in
    J|JA|Ja|j|ja|Y|YES|Yes|y|yes)
      SHOWTEXTJANEIN=YES
      ;;
    *)
      SHOWTEXTJANEIN=NO
      ;;
  esac
else
  SHOWTEXTJANEIN=NO
fi

# Text zum Einblenden
# Steht in Variable ${TEXT}

# Zeitstempel einblenden ja/nein
if [ ! "${SHOWZEITSTEMPELJANEIN}xxx" = "xxx" ] ; then
  case "${SHOWZEITSTEMPELJANEIN}" in
    J|JA|Ja|j|ja|Y|YES|Yes|y|yes)
      SHOWZEITSTEMPELJANEIN=YES
      ;;
    *)
      SHOWZEITSTEMPELJANEIN=NO
      ;;
  esac
else
  SHOWZEITSTEMPELJANEIN=NO
fi

# Textfarbe, default black
# Erlaubt sind red, yellow, blue, green, white, black
if [ ! "${TEXTFARBE}xxx" = "xxx" ] ; then
  case "${TEXTFARBE}" in
    RED|red)
      TEXTFARBE="red"
      ;;
    YELLOW|yellow)
      TEXTFARBE="yellow"
      ;;
    BLUE|blue)
      TEXTFARBE="blue"
      ;;
    GREEN|green)
      TEXTFARBE="green"
      ;;
    WHITE|white)
      TEXTFARBE="white"
      ;;
    *)
      TEXTFARBE="black"
      ;;
  esac
else
  TEXTFARBE="black"
fi

# Textgroesse, default 26
if [ ! "${TEXTGROESSE}xxx" = "xxx" ] ; then
  TEXTSIZE=$(echo "${TEXTGROESSE}" | sed 's/[^0-9]//g')
  [ -z "${TEXTSIZE}" ] && TEXTSIZE=26
else
  TEXTSIZE=26
fi

# Bild/Logo einblenden, wenn vorhanden dann LOGOBREITE (default 100) auswerten
LOGOPARAM=""
if [ ! "${LOGODATEINAME}xxx" = "xxx" ] ; then
  if [ -r "/boot/${LOGODATEINAME}" ] ; then
    LOGOPARAM=" -i /boot/${LOGODATEINAME} "
    if [ ! "${LOGOBREITE}xxx" = "xxx" ] ; then
      LOGOBREITE=$(echo "${LOGOBREITE}" | sed 's/[^0-9]//g')
      [ -z "${LOGOBREITE}" ] && LOGOBREITE=100
    else
      LOGOBREITE=100
    fi
    # Text kommt immer ueber dem Logo und die Logo-Position verschiebt sich je nach Textgroesse
    # Ohne Text muss die Textgroesse 5 sein (fuer 10px Mindestabstand)
    if [ "${SHOWTEXTJANEIN}" = "NO" -a "${SHOWZEITSTEMPELJANEIN}" = "NO" ] ; then
      TEXTSIZE=5
    fi
  else
    echo "Bilddatei /boot/${LOGODATEINAME} nicht gefunden. $(/bin/date)" >> "${YOUTUBE_FFMPEG_LOGFILE}"
  fi
fi

# Position Text und Logo 
# Erlaubt sind topleft, topright, bottomleft, bottomright
# default ist topleft
if [ "${SHOWTEXTJANEIN}" = "YES" -o "${SHOWZEITSTEMPELJANEIN}" = "YES" -o ! "${LOGOPARAM}xxx" = "xxx" ] ; then
  if [ ! "${TEXTPOSITION}xxx" = "xxx" ] ; then
    case "${TEXTPOSITION}" in
      topright)
        # Oben rechts
        TEXTPOSITION="x=main_w-tw-10:y=10"
        LOGOPOSITION="-1 [ovrl],[1:v][ovrl]overlay=main_w-overlay_w-10:$((TEXTSIZE * 2))"
        ;;
      bottomleft)
        # Unten links
        TEXTPOSITION="x=10:y=main_h-th-10"
        LOGOPOSITION="-1 [ovrl],[1:v][ovrl]overlay=10:main_h-overlay_h-$((TEXTSIZE * 2))"
        ;;
      bottomright)
        # Unten rechts
        TEXTPOSITION="x=main_w-tw-10:y=main_h-th-10"
        LOGOPOSITION="-1 [ovrl],[1:v][ovrl]overlay=main_w-overlay_w-10:main_h-overlay_h-$((TEXTSIZE * 2))"
        ;;
      *)
        # Default Oben links
        TEXTPOSITION="x=10:y=10"
        LOGOPOSITION="-1 [ovrl],[1:v][ovrl]overlay=10:$((TEXTSIZE * 2))"
        ;;
    esac
  else
    TEXTPOSITION="x=10:y=10"
    LOGOPOSITION="-1 [ovrl],[1:v][ovrl]overlay=10:$((TEXTSIZE * 2))"
  fi
fi

# Los geht's mit der Endlosschleife fuer ffmpeg ...
while true
do

  # Logdatei max. 10MB (=10000 blocks). Wenn groesser, dann loesche die ersten 1000 Zeilen
  [ $(ls --size "${YOUTUBE_FFMPEG_LOGFILE}" | cut -d ' ' -f1) -gt 10000 ] && sed -i '1,1000d' "${YOUTUBE_FFMPEG_LOGFILE}"

  # ffmpeg-Filterparameter zusammenbauen
  FILTERPARAMS=""
  if [ "${SHOWTEXTJANEIN}" = "YES" -o "${SHOWZEITSTEMPELJANEIN}" = "YES" -o ! "${LOGOPARAM}xxx" = "xxx" ] ; then
    # Syntax: VARIABLE+ erzeugt/fuellt ein Array, welches mit ${VARIABLE[@]} komplett als Zeichenkette ausgegeben werden kann
    FILTERPARAMS='-filter_complex "'
    # Mit Logo
    if [ ! "${LOGOPARAM}xxx" = "xxx" ] ; then
      FILTERPARAMS+="[2:v]scale=${LOGOBREITE}:${LOGOPOSITION}"
      if [ "${SHOWTEXTJANEIN}" = "YES" -o "${SHOWZEITSTEMPELJANEIN}" = "YES" ] ; then
        FILTERPARAMS+=" , "
      fi  
    fi
    # Mit Text
    if [ "${SHOWTEXTJANEIN}" = "YES" -o "${SHOWZEITSTEMPELJANEIN}" = "YES" ] ; then
      FILTERPARAMS+=" drawtext=fontfile=/usr/share/fonts/truetype/dejavu/DejaVuSerif-Bold.ttf:text='"
    fi  
    if [ "${SHOWTEXTJANEIN}" = "YES" ] ; then
      FILTERPARAMS+="${TEXT}"
    fi
    # Mit Zeitstempel
    if [ "${SHOWZEITSTEMPELJANEIN}" = "YES" ] ; then
      # Die localtime muss in Sekunden seit Unix-Startzeit angegeben werden, und zwar 
      # jedesmal, wenn ffmpeg gestartet wird.
      # Die vielen \ (Backslashes) muessen sein fuer das Escapen in den Subshells ...
      FILTERPARAMS+="%{pts\\:localtime\\:$(date +"%s")\\:%d.%m.%Y, %H\\\\\\\\\:%M\\\\\\\\\:%S} Uhr'"
    fi
    # Farbe und Groesse
    if [ "${SHOWTEXTJANEIN}" = "YES" -o "${SHOWZEITSTEMPELJANEIN}" = "YES" ] ; then
      FILTERPARAMS+=":fontcolor=${TEXTFARBE}:fontsize=${TEXTSIZE}:${TEXTPOSITION}"
    fi  
    if [ "${SPECIALFILTER}xxx" = "xxx" ] ; then
      FILTERPARAMS+='"'
    fi
  fi


  # Extra-Filterparameter für ffmpeg aus der /boot/config_youtube_lifestream.txt Datei ...
  if [ ! "${SPECIALFILTER}xxx" = "xxx" ] ; then
    if [ "${FILTERPARAMS}xxx" = "xxx" ] ; then
      FILTERPARAMS+='-filter_complex "'"${SPECIALFILTER}"'"'
    else
      FILTERPARAMS+=" , ${SPECIALFILTER}"'"'
    fi
  fi

  # Beispiel-ffmpeg-Aufruf direkt in der Konsole (mit der passenden Anzahl Escape-Zeichen)
  # ffmpeg -loglevel warning -re -f s16le -i /dev/zero -f v4l2 -thread_queue_size 128 -video_size 960x720 -i /dev/video0 -i /home/pi/blaumeise.png -c:v h264_omx -filter_complex "[2:v]scale=100:-1 [ovrl],[1:v][ovrl]overlay=main_w-overlay_w-10:main_h-overlay_h-50, drawtext=fontfile=/usr/share/fonts/truetype/dejavu/DejaVuSerif-Bold.ttf:text='BirdBnB @ B7 | %{pts\:localtime\:$(date +"%s")\:%d.%b.%Y, %H\\\\\:%M\\\\\:%S} Uhr':fontcolor=blue:fontsize=36:x=main_w-tw-10:y=main_h-th-10" -b:v 700k -r 20 -q 50 -acodec aac -ab 32k -f flv rtmp://a.rtmp.youtube.com/live2/67j7-g64x-rwuy-fbgv
  # ffmpeg -loglevel warning -hide_banner -nostats -re -f s16le -i /dev/zero -f v4l2 -thread_queue_size 128 -video_size 960x720 -i /dev/video0  -c:v h264_omx -filter_complex " drawtext=fontfile=/usr/share/fonts/truetype/dejavu/DejaVuSerif-Bold.ttf:text='%{pts\:localtime\:1614112374\:%d.%m.%Y, %H\\\\\:%M\\\\\:%S} Uhr':fontcolor=yellow:fontsize=32:x=main_w-tw-10:y=main_h-th-10" -b:v 700k -r 20 -q 50 -acodec aac -ab 32k -f flv rtmp://a.rtmp.youtube.com/live2/vrzy-5eqg-vqdf-9dpg-7qqe

  # Mitloggen zum Testen ...
  #echo "ffmpeg-Aufruf: ffmpeg -hide_banner -nostats -re -f s16le -i /dev/zero -f v4l2 -thread_queue_size 128 -video_size 960x720 -i /dev/video0 ${LOGOPARAM} -c:v h264_omx ${FILTERPARAMS[@]} -b:v 700k -r 20 -q 50 -acodec aac -ab 32k -f flv ${STREAMURL}"  >> "${YOUTUBE_FFMPEG_LOGFILE}"

  # Mit Debian11 wurde h264_omx entfernt, dafuer soll h264_v4l2m2m verwendet werden. Das laeuft aber leider nicht stabil.
  # h264_omx funktioniert also nur mit Debian10 basierten Installations-Images (bis 2021-05-28)
  # Wer es trotzdem probieren moechte (Debian11): Einfach "-c:v h264_omx" mit "-c:v h264_v4l2m2m" ersetzen.

  # Und jetzt endlich das ffmpeg-Kommando ...
  eval ffmpeg -hide_banner -nostats \
      -re -f s16le -i /dev/zero \
      -f v4l2 -thread_queue_size 128 -video_size 960x720 -i /dev/video0 \
      ${LOGOPARAM} \
      -c:v h264_omx \
      ${FILTERPARAMS[@]} \
      -b:v 700k -r 20 -q 50 -acodec aac -ab 32k -f flv \
      ${STREAMURL}

  sleep 5
  echo "Restart ffmpeg: $(/bin/date)" >> "${YOUTUBE_FFMPEG_LOGFILE}"
done

